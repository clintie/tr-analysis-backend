## Triad analysis backend

v0.01 Initial Release

v0.02 Redux enhancements

## Summary

Here, we manage the connection to our locally saved National Grid data 
scraped from Elexon data placed it into the [REDACTED] database.

This data can be updated every minute and therefore the URL endpoints will
be polled every 10 seconds.

Essentially here we offer programmatic access to a series of PostgreSQL SELECT
queries which offer custom WHERE clause access via the ExpressJS API's endpoints.  