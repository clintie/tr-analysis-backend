
const CONFIG = {
    pass:   '[REDACT]',
    server: '[REDACT]:5432',
    db:     '[REDACT]',
    user:   '[REDACT]'
}
module.exports = CONFIG;