var config = require('../config/config');
var https = require('https');
var convert = require('xml-js');

var express = require('express');
var router = express.Router();

const pg = require('pg');
const path = require('path');
const connectionString = `postgres://${config.user}:${config.pass}@${config.server}/${config.db}`;


/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Triad Analysis Server Backend' });
});

router.get('/api/genfuel/:date/:focus', (req, res, next) => {
  const results = [];
  const focus= req.params.focus == 'true';
  const startTime = focus ? '14:25' : '00:00';
  const endTime = focus ? '20:00' : '23:59';

  // Get a Postgres client from the connection pool
  if ((!req.params.date.length) || (!isValidDate(req.params.date)) ) {
    return res.status(500).json({success: false, data: 'err'});
  }

  pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    // SQL Query > Select Data
    const query = client.query(`
      SELECT publish_ts, sum(mw)
      FROM genbyfueltype_inst
      where report_ts >= '${req.params.date} ${startTime}'
      and report_ts <= '${req.params.date} ${endTime}'
      and fueltype_id > -1
      group by publish_ts
      order by publish_ts
    `);
    // Stream results back one row at a time
    query.on('row', (row) => {
      results.push(row);
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
      done();
      return res.json(results);
    });
  });
});


router.get('/api/sd/:timestamp/:focus', (req, res, next) => {
  const results = [];
  const focus= req.params.focus == 'true';
  const focusCriteria = focus? ' and settlement_period > 29 and settlement_period < 41 ' : 
                                ' and settlement_period > 0 and settlement_period < 49'
  
  // Get a Postgres client from the connection pool
  if ((!req.params.timestamp.length) || (!isValidDate(req.params.timestamp)) ) {
    return res.status(500).json({success: false, data: 'err'});
  }

  pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    // SQL Query > Select Data
    const query = client.query(`
      SELECT settlement_day, settlement_period, demand_forecast, demand_outturn, 
      report_ts
      FROM scraping.public.systemdemand
      where settlement_day = TIMESTAMP '${req.params.timestamp}' ${focusCriteria}
      order by settlement_period;
    `);
    // Stream results back one row at a time
    query.on('row', (row) => {
      results.push(row);
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
      done();
      return res.json(results);
    });
  });
});


router.get('/api/fcast/:timestamp/:focus', (req, res, next) => {
  const results = [];
  const focus= req.params.focus == 'true';
  const focusCriteria = focus? ' and settlement_period > 29 and settlement_period < 41 ' : 
                                ' and settlement_period > 0 and settlement_period < 49'
  
  // Get a Postgres client from the connection pool
  if ((!req.params.timestamp.length) || (!isValidDate(req.params.timestamp)) ) {
    return res.status(500).json({success: false, data: 'err'});
  }

  pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
    if (err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    // SQL Query > Select Data
    const query = client.query(`
      (WITH last_forecast AS (
        SELECT settlement_day, settlement_period,  MAX(report_ts) AS report_ts
          FROM scraping.public.systemdemand_forecast
          where settlement_day = TIMESTAMP '${req.params.timestamp}' ${focusCriteria}
          group by settlement_day, settlement_period
      )
      SELECT * FROM scraping.public.systemdemand_forecast
        JOIN last_forecast USING (settlement_day, settlement_period, report_ts))
        order by settlement_period
    `);
    // Stream results back one row at a time
    query.on('row', (row) => {
      results.push(row);
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
      done();
      return res.json(results);
    });
  });
});

router.get('/api/sdr/:timestamp/:focus', (req, res, next) => {
  const results = [];
  const focus= req.params.focus == 'true';
  const focusStart = focus? ` + INTERVAL '14 hours' + INTERVAL '30 minutes' ` : ` + INTERVAL '0 hours' + INTERVAL '0 minutes' `
  const focusEnd = focus? ` + INTERVAL '20 hours' + INTERVAL '0 minutes' ` : ` + INTERVAL '23 hours' + INTERVAL '30 minutes' `
  console.log('focus:', focus)
  // Get a Postgres client from the connection pool
  if ((!req.params.timestamp.length) || (!isValidDate(req.params.timestamp)) ) {
    return res.status(500).json({success: false, data: 'err'});
  }

  pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    // SQL Query > Select Data
    const query = client.query(`
      select t1.report_ts as ts,t2.rolling_tsd as rolling_tsd,t1.rolling_triad_demand  as rolling_triad_demand from
      (
      SELECT report_ts,sum(mw) as rolling_triad_demand
        FROM genbyfueltype_inst
      where report_ts between
      TIMESTAMP '${req.params.timestamp}' ${focusStart}
      and TIMESTAMP '${req.params.timestamp}' ${focusEnd}
        group by report_ts
      )  t1
        join (
      SELECT report_ts,sum(mw) as rolling_tsd
        FROM genbyfueltype_inst
      where report_ts between
      TIMESTAMP '${req.params.timestamp}' ${focusStart}
      and TIMESTAMP '${req.params.timestamp}' ${focusEnd}
        and mw > 0
        group by report_ts
      )  t2
      on t1.report_ts = t2.report_ts
    `);
    // Stream results back one row at a time
    query.on('row', (row) => {
      results.push(row);
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
      done();
      return res.json(results);
    });
  });
});


router.get('/api/ic/:timestamp/:focus', (req, res, next) => {
  const results = [];
  const focus= req.params.focus == 'true';
  const focusCriteria = focus? ' and hh.ft_period > 29 and hh.ft_period < 41 ' : ' and hh.ft_period > 0 and hh.ft_period < 49 '

  // Get a Postgres client from the connection pool
  if ((!req.params.timestamp.length) || (!isValidDate(req.params.timestamp)) ) {
    return res.status(500).json({success: false, data: 'err'});
  }

  pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    // SQL Query > Select Data
    const query = client.query(`
    SELECT fueltype.fuel_type, hh.ft_period as settlement_period, hh.mw
    FROM genbyfueltype_hh as hh
    join fueltype on fueltype.id = hh.fueltype_id
      where hh.ft_date = '${req.params.timestamp}' ${focusCriteria}
      and fueltype.interconnect
    `);
    // Stream results back one row at a time
    query.on('row', (row) => {
      results.push(row);
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
      done();
      return res.json(results);
    });
  });
});

router.get('/api/ngrt/:timestamp/:focus', (req, res, next) => {
  const results = [];
  const focus= req.params.focus == 'true';
  const startTime = focus ? '14:25' : '00:00';
  const endTime = focus ? '20:00' : '23:59';


  if ((!req.params.timestamp.length) || (!isValidDate(req.params.timestamp)) ) {
    return res.status(500).json({success: false, data: 'err'});
  }

  pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }

    const query = client.query(`select * from ngrt_demand 
    where report_ts BETWEEN
    TIMESTAMP '${req.params.timestamp} ${startTime}'
    and TIMESTAMP '${req.params.timestamp} ${endTime}'
    `);

    // Stream results back one row at a time then close the stream and return results
    query.on('row', row => results.push(row));
    query.on('end', () => {
      done();
      return res.json(results);
    });
  });
});


function isValidDate(dateString) {
  var regEx = /^\d{4}-\d{2}-\d{2}$/;
  if(!dateString.match(regEx)) return false;  // Invalid format
  
  var d = new Date(dateString);
  if(Number.isNaN(d.getTime())) return false; // Invalid date

  return d.toISOString().slice(0,10) === dateString;
}

module.exports = router;
